# README #

This is a gnuplot helper wrapper that is intended to allow users that are unfamiliar with gnuplot's instructions and syntax to take some advantage of it when it comes to generating simple graphs from data files.

### Features ###

* auto-detection of the data separator / column delimiter
* auto-detection of the date/time format (if present)
* auto-detection of data columns in file
* auto-detection of the output file name (input file with .png extension)
* supports multiple input files (provided that they all have the same date/time format)
* supports data via standard in (i.e. you can pipe data to it as in `grep ... | plot.py`)
* prints an ascii version of the plot on the command line before saving as a .png
* additional formatting options via command line arguments

### How do I get set up? ###

* simply download and run on the command line
* get started by running `plot.py --help`

### License ###

* Simplified BSD License/FreeBSD License (GPL-compatible free software license)

### Requirements ###

* Linux
* gnuplot
* Python v2.6.x or greater (might work on earlier versions)