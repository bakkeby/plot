#!/usr/bin/env python
# coding=UTF8
# APPTAG|python|plot.py|A gnuplot helper wrapper to generate quick graphs from data files
# Copyright (c) 2016, Stein Gunnar Bakkeby, all rights reserved.

#
# If you find yourself faced with errors like this:
#  "Could not find/open font when opening font "arial", using internal non-scalable font"
#
# Then you may need to set these two env variables to something sensible:
#
# export GDFONTPATH="/usr/share/fonts/liberation/"
# export GNUPLOT_DEFAULT_GDFONT="LiberationSans-Regular"
#

import argparse
import subprocess
import sys
import re
import codecs
import ntpath
import os
import random
import getpass
import time
import logging

show_more_args = '--more' in sys.argv

time_re = re.compile("^[0-9/:. -]{5,}$")
numb_re = re.compile("^[0-9.]+$")

def deduce_delimiter_from_lines(lines):

	del_idx = 0
	delim_col_count = []
	delimiters = ['|', ',', ';', '=', '	', ' ']
	
	for del_idx in range(len(delimiters)):
		delim_col_count.append([])
		delimiter = delimiters[del_idx]
		
		for line in lines:
			columns = line.split(delimiter)
			
			num_numb_col = 0
			num_time_col = 0
			num_othr_col = 0
			
			for col in columns:
				if numb_re.match(col):
					num_numb_col += 1
				elif time_re.match(col):
					num_time_col += 1
				else:
					num_othr_col += 1
			
			num_list = [num_numb_col, num_time_col, num_othr_col]
			
			if num_list not in delim_col_count[del_idx]:
				delim_col_count[del_idx].append(num_list)
			
		del_idx += 1
		
	max_numb = 0
	delimiter = ''
	
	for del_idx in range(len(delimiters)):
		num_lists = delim_col_count[del_idx]
		if len(num_lists) != 1:
			continue
		
		numb, time, othr = num_lists[0]

		if numb + time > max_numb:
			max_numb = numb + time
			delimiter = delimiters[del_idx]
		
	return delimiter

def deduce_plot_columns_from_lines(lines, delimiter):
	
	rows = []
	for line in lines:
		rows.append(line.split(delimiter))
	
	# Select the first time column found for the x column
	time_cols = []
	numb_cols = []

	# The number of columns is determined by the first row
	num_cols = len(rows[0])
	uneven_cols_warning_logged = False
	for c in range(num_cols):
		
		time_rows_matched = 0
		numb_rows_matched = 0
		col_values = []
		for row in rows:
			if c >= len(row):
				if uneven_cols_warning_logged == False:
					logging.warning("Uneven number of columns detected for line: " + delimiter.join(row))
					uneven_cols_warning_logged = True
				continue
			
			col = row[c]
			
			if numb_re.match(col):
				numb_rows_matched += 1
				if col not in col_values:
					col_values.append(col)
			elif time_re.match(col):
				time_rows_matched += 1
	
		if time_rows_matched == len(rows):
			time_cols.append(c)
		# If the column contains numbers then expect at least 10% variation in the numbers listed
		# I.e. ignore columns having the same value
		elif numb_rows_matched == len(rows) and len(col_values) > len(rows) * 10/100:
			numb_cols.append(c)

	plot_columns = []
	if len(time_cols) > 0:
		for i in range(len(numb_cols)):
			x_col = time_cols[0] + 1
			y_col = numb_cols[i] + 1
			plot_columns.append(str(x_col) + ":" + str(y_col))
	elif len(numb_cols) == 1:
		# note that gnuplot will treat the file line numbers as the x column if only one value is provided
		y_col = numb_cols[0] + 1
		plot_columns.append(str(y_col))
	else:
		for i in range(1,len(numb_cols)):
			x_col = numb_cols[0] + 1
			y_col = numb_cols[i] + 1
			plot_columns.append(str(x_col) + ":" + str(y_col))
	
	return plot_columns

def deduce_input_time_format_from_time(time):
	
	time_formats = {
		"^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$":      "%Y-%m-%d %H:%M:%S",
		"^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$":               "%Y-%m-%d %H:%M",
		"^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}$":                        "%Y-%m-%d %H",
		"^[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}$":                        "%Y-%m-%d_%H",
		"^[0-9]{4}-[0-9]{2}-[0-9]{2}$":                                 "%Y-%m-%d",
		"^[0-9]{4}-[0-9]{2}$":                                          "%Y-%m",
		"^[0-9]{2}/[0-9]{2}-[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}$":     "%m/%d-%H:%M:%S",
		"^[0-9]{2}/[0-9]{2}-[0-9]{2}:[0-9]{2}:[0-9]{2}$":               "%m/%d-%H:%M:%S",
		"^[0-9]{2}/[0-9]{2}-[0-9]{2}:[0-9]{2}$":                        "%m/%d-%H:%M",
		"^[0-9]{2}/[0-9]{2}-[0-9]{2}$":                                 "%m/%d-%H",
		"^[0-9]{2}/[0-9]{2}$":                                          "%m/%d",
		"^[0-9]{2}:[0-9]{2}:[0-9]{2}$":                                 "%H:%M:%S",
		"^[0-9]{2}:[0-9]{2}$":                                          "%H:%M",
		"^[0-9]{8}_[0-9]{2}$":                                          "%Y%m%d_%H"
	}
	
	timefmt = ""
	
	for regexp in time_formats:
		tfmt_re = re.compile(regexp)
		if tfmt_re.match(time):
			timefmt = time_formats[regexp]
			break
		
	return timefmt
	

def deduce_settings_from_input_file(args):
	
	f_idx = 0
	for file in args.input:
		num_lines_to_check = 20
		lines = []
		with open(file, 'r') as f:
			for i in range(num_lines_to_check):
				line = f.readline().strip()
				if line == "":
					break
				lines.append(line)
			
		# Deduce delimiter
		if len(args.delimiter) <= f_idx:
			delimiter = deduce_delimiter_from_lines(lines)

			if delimiter == '':
				logging.warning("File " + file + ": Unable to deduce delimiter based on line: " + lines[0])
				logging.warning("Please specify using the --delimiter parameter")
				exit(0)
			else:
				logging.debug("File " + file + ": Deduced delimiter of \"" + delimiter + "\" based on line: " + lines[0])
				args.delimiter.append(delimiter)
			
		delimiter = args.delimiter[f_idx]
		
		# Deduce columns
		if len(args.plot_columns) <= f_idx:
			plot_columns = deduce_plot_columns_from_lines(lines, delimiter)
			if len(plot_columns) == 0:
				logging.warning("File " + file + ": Unable to deduce columns based on line: " + lines[0])
				logging.warning("Please specify using the -c parameter")
				exit(0)
			logging.debug("File " + file + ": Deduced plot columns of \"" + ", ".join(plot_columns) + "\" based on line: " + lines[0])
			args.plot_columns.append(plot_columns)
		else:
			args.plot_columns[:] = [re.sub(r'[^0-9]+', ':', s) for s in args.plot_columns]
			
		plot_str = args.plot_columns[f_idx][0]
			
		# Deduce timeformat
		if len(args.timefmt) <= f_idx:
			time_col = plot_str.split(":")[0]
			x_col = int(time_col)-1
			x_val = lines[0].split(delimiter)[x_col]
			
			timefmt = ""
			# The x column may not necessarily contain time data, could be numbers
			if time_re.match(x_val):
				timefmt = deduce_input_time_format_from_time(x_val)
				if timefmt != "":
					logging.debug("File " + file + ": Deduced time format of \"" + timefmt + "\" based on line: " + lines[0])
				
			args.timefmt.append(timefmt)
		
		# Deduce vertical (y) tics / format
		if args.yfmt == "":
			plot_lst = plot_str.split(":")
			value_col = plot_lst[1 if len(plot_lst) > 1 else 0]
			y_col = int(value_col)-1
			y_val = lines[0].split(delimiter)[y_col]
			# This is a bit convoluted, but the gist of it is that if we are working with
			# values that exceed one million then gnuplot defaults to using scientific notation
			# e.g. 1.5644e+07 instead of 15644000. This just tries to detect large numbers and
			# set the vertical tics to be whole numbers.
			if re.match(r'^[0-9]{7,}', y_val):
				args.yfmt = '%.0f'
			
		f_idx += 1
		
	return args

# have a look at http://gnuplot.info/demo/
def generate_plot(args):
	
	args = deduce_settings_from_input_file(args)
	
	cmds = []
	cmds.append("set terminal dumb")
	
	for setting in ['xlabel', 'ylabel', 'title', 'xdata']:
		value = eval("args." + setting)
		if value != "":
			cmds.append("set " + setting + " \"" + value + "\"")
	
	for setting in ['key', 'xtics', 'grid']:
		value = eval("args." + setting)
		if value != "":
			cmds.append("set " + setting + " " + value)

	f_idx = 0

	cmds.append("set datafile separator \"" + args.delimiter[f_idx] + "\"")
	
	if args.timefmt[f_idx] != "":
		cmds.append("set timefmt \"" + args.timefmt[f_idx] + "\"")
		cmds.append("set xdata time")
		if args.xfmt == "" and args.timefmt[f_idx] in ["%H:%M", "%M:%S"]:
			cmds.append("set format x \"" + args.timefmt[f_idx] + "\"")
	
	if args.xfmt != "":
		cmds.append("set format x \"" + args.xfmt + "\"")
		
	if args.yfmt != "":
		cmds.append("set format y \"" + args.yfmt + "\"")
		
	if args.ymin != "" and args.ymax != "":
		cmds.append("set yrange [" + args.ymin + ":" + args.ymax + "]")

	plot_cmd_list = []
	l_idx = 0
	i = 0
	for plot_column in args.plot_columns:
		# curious side effect of appending to lists via argparse, presumably a bug
		if type(plot_column) is list:
			plot_column = plot_column[0]
		if len(args.label) > l_idx:
			label = args.label[l_idx]
			l_idx += 1
		else:
			plot_lst = plot_column.split(":")
			label = "col " + plot_lst[1 if len(plot_lst) > 1 else 0]
			if len(set(args.input)) > 1:
				label = "file " + chr(65 + i) + " " + label
		
		plot_cmd_list.append("\"" + args.input[i] + "\" using " + plot_column + " title \"" + label + "\" with " + args.plot_style)
		
		if i + 1 < len(args.input):
			i += 1
	
	if args.linear_regression:
		plot_columns = args.plot_columns[f_idx][0]
		if len(plot_columns.split(":")) == 1:
			logging.debug("Omitting linear regression calculation as there are not enough columns")
		else:
			cmds.append("set fit quiet")
			cmds.append("set fit logfile \"/dev/null\" errorvariables")
			cmds.append("set multiplot")
			cmds.append("f(x) = b+m*x")
			cmds.append("set autoscale x")
			cmds.append("fit f(x) \"" + args.input[f_idx] + "\" using " + plot_columns + " via b,m")
			plot_cmd_list.append(" f(x) title \"linear regression\" lt rgb \"black\"")
	
	cmds.append("plot " + ", ".join(plot_cmd_list))
	
	params = "'" + ";".join(cmds) + "'"
	cmd = "gnuplot -e " + params
	
	logging.debug("Executing command: " + cmd)
	sp = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout = sp.stdout.read()
	stderr = sp.stderr.read()
	
	if False and len(stderr) > 0:
		logging.error("Sorry, unfortunately gnuplot didn't seem to like that :(")
		logging.error("-" * 80)
		logging.error(stderr)
		logging.error("-" * 80)
	elif args.terminal == "dumb":
		logging.info(stdout)
	else:
		logging.info(stdout)
		
		if args.output == "":
			input    = args.input[0]
			filename = ntpath.basename(input) + "." + args.terminal
			args.output = re.sub(r'\.[0-9A-Za-z]+\.'+args.terminal+'$',"." + args.terminal,filename)
		
		cmds[0] = "set terminal " + args.terminal
		if args.terminal_size != "":
			cmds[0] += " size " + args.terminal_size
		
		cmds.insert(0, "set output \"" + args.output + "\"")
		
		if logging.getLogger().isEnabledFor(logging.DEBUG):
			logging.debug("The gnuplot commands used to generate the output image:")
			logging.debug("```")
			for cmd in cmds:
				logging.debug(cmd)
			logging.debug("```")

		params = "'" + ";".join(cmds) + "'"
		cmd = "gnuplot -e " + params
		
		logging.debug("Executing command: " + cmd)
		sp = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout = sp.stdout.read()
		stderr = sp.stderr.read()
		if len(stderr) > 0:
			logging.error("Sorry, unfortunately gnuplot didn't seem to save that properly :(")
			logging.error("-" * 80)
			logging.error(stderr)
			logging.error("-" * 80)
		else:
			logging.info("Plot saved as " + args.output)
		
		if len(stdout) > 0:
			logging.info(stdout)

#
# Set up application parameters for argument parsing.
#
# @return          instance of argparse.ArgumentParser
#
def get_argument_parser():
	
	tips = [
		'Tip: Did you know that you can customise your default settings in ~/.gnuplot?',
		'Tip: Check out http://gnuplot.info/demo/ for impressive examples of what gnuplot can do.',
		'Tip: http://gnuplot.info/docs_5.0/gnuplot.pdf is an excellent reference if you are serious about gnuplot.',
		'Warning! Plotting to outplot the plot of fellow plotters will only results in counterplot.',
		'Tip: Did you know that this script supports input from standard in? You can just pipe data to it.'
	]
	
	parser = argparse.ArgumentParser(
		description="This is a gnuplot helper wrapper that aims to simplify things when it comes to generating graphs from data files. If more than one data file is being used to plot different lines in the generated graph then they all need to have the same date/time formats and data separators (gnuplot restriction).",
		formatter_class=argparse.RawDescriptionHelpFormatter,
		epilog=random.choice(tips)
	)
	parser.add_argument(
		"-i", "--input",
		default=[],
		dest='input',
		metavar="<file>",
		help="one or more input files to include in the plotted graph",
		action='append'
	)
	parser.add_argument(
		"-o", "--output",
		default="",
		dest='output',
		metavar="<file>",
		help="name of the output file to store the plotted graph (defaults to <input>.png)",
		action='store'
	)
	parser.add_argument(
		"-d", "--delimiter",
		default=[],
		dest='delimiter',
		metavar="<sep>",
		help="the delimiter that separates columns in the input file, if not specified the script will attempt to deduce it based on the file contents",
		action='append'
	)
	parser.add_argument(
		"-c", "--columns",
		default=[],
		dest='plot_columns',
		metavar="<x:y>",
		help="the columns to plot on the form ?<int>:?<int>, e.g. 1:2 where the horizontal column is the first one in the input file and the value is in the second column. The separator of \":\" here is just for familiarity with gnuplot's way of representing columns, but any non-digit character can be used, i.e. 1,2 works the same. Note that if a single column is selected then that will have to be a numeric value and the x value will default to be the line numbers in the file.",
		action='append'
	)
	parser.add_argument(
		"-l", "--label",
		default=[],
		dest='label',
		metavar="<name>",
		help="the name of the line being plotted, defaults either to \"col #\" or \"file # col #\" depending on whether multiple files are being used. For example \"col 2\" means that the line represents the second column of the input data.",
		action='append'
	)
	parser.add_argument(
		"--title",
		default="",
		dest='title',
		metavar="<string>",
		help="specify the title of the plot (defaults to not being set)",
		action='store'
	)
	parser.add_argument(
		"--xlabel",
		default="",
		dest='xlabel',
		metavar="<string>",
		help="set the x-label (defaults to not being set)",
		action='store'
	)
	parser.add_argument(
		"--ylabel",
		default="",
		dest='ylabel',
		metavar="<string>",
		help="set the y-label (defaults to not being set)",
		action='store'
	)
	parser.add_argument(
		"--style",
		default="lines",
		dest='plot_style',
		metavar="<style>",
		help="the style of the plotted graph, e.g. lines, points, impulses, etc. For additonal styles refer to: http://lowrank.net/gnuplot/intro/style-e.html",
		action='store'
	)
	parser.add_argument(
		"--more",
		help=argparse.SUPPRESS if show_more_args else "show advanced options",
		action="store_true"
	)
	parser.add_argument(
		"--xdata",
		default="",
		dest='xdata',
		metavar="<type>",
		help="define what the xdata represents (typically time)" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--xfmt",
		default="",
		dest='xfmt',
		metavar="<format>",
		help="the time format to display in the plot output, defaults to letting gnuplot decide" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--yfmt",
		default="",
		dest='yfmt',
		metavar="<format>",
		help="the number format to display in the plot output, defaults to letting gnuplot decide" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"-t", "--timefmt",
		default=[],
		dest='timefmt',
		metavar="<format>",
		help="specify the time format being used in the input file" if show_more_args else argparse.SUPPRESS,
		action='append'
	)
	parser.add_argument(
		"--ls", "--legend_style",
		default="",
		dest='key',
		metavar="<style>",
		help="Enables a legend describing plots on a plot. For options see: http://gnuplot.sourceforge.net/docs_4.2/node192.html" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--ymin",
		default="",
		dest='ymin',
		metavar="<num>",
		help="sets the minimum value for the Y axis" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--ymax",
		default="",
		dest='ymax',
		metavar="<num>",
		help="sets the maximum value for the Y axis" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--lr", "--linear_regression",
		default=False,
		dest='linear_regression',
		help="include a linear regression line in the output (for the first file only)" if show_more_args else argparse.SUPPRESS,
		action='store_true'
	)
	parser.add_argument(
		"--info",
		default=logging.INFO,
		const=logging.INFO,
		dest='log_level',
		help=argparse.SUPPRESS, # supressed because it is the default log level
		action='store_const'
	)
	parser.add_argument(
		"--quiet",
		const=logging.ERROR,
		dest='log_level',
		help="run the script in \"quiet\" mode, i.e. reduce output of non-vital information" if show_more_args else argparse.SUPPRESS,
		action='store_const'
	)
	parser.add_argument(
		"--debug",
		const=logging.DEBUG,
		dest='log_level',
		help="enable debug logging" if show_more_args else argparse.SUPPRESS,
		action='store_const'
	)
	parser.add_argument(
		"--dry-run",
		default=False,
		dest='dry_run',
		help="do a dry-run, i.e. run the script, but don't make any changes" if show_more_args else argparse.SUPPRESS,
		action='store_true'
	)
	parser.add_argument(
		"--keep",
		default=False,
		dest='keep_temporary_files',
		help="keep temporary files (i.e. do not delete them)" if show_more_args else argparse.SUPPRESS,
		action='store_true'
	)
	parser.add_argument(
		"--tics",
		default="",
		dest='xtics',
		metavar="<xtics>",
		help="fine control of the major (labelled) tics on the x-axis, for details refer to: http://gnuplot.sourceforge.net/docs_4.2/node295.html" if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"-g", "--grid",
		default="",
		const="x y",
		dest='grid',
		help="show a background grid" if show_more_args else argparse.SUPPRESS,
		action='store_const'
	)
	parser.add_argument(
		"--terminal",
		default="png",
		dest='terminal',
		metavar="<type>",
		help="defines the terminal being used, i.e. specifies the output format of the generated plot or graph. The command \"set terminal\" in a gnuplot console shows a list of available types. If set to \"dumb\" then no file is stored. Defaults to \"png\", i.e. storing the rendered graph as a .png image file." if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	parser.add_argument(
		"--terminal_size",
		default="1600,900",
		dest='terminal_size',
		metavar="<x,y>",
		help="defines the terminal output size to use, i.e. specifies the height and width of the rendered plot or graph. Set to empty string to let gnuplot decide. Defaults to \"1600,900\"." if show_more_args else argparse.SUPPRESS,
		action='store'
	)
	
	parser.add_argument(
		"remaining_args",
		nargs='*',
		help=argparse.SUPPRESS
	)
	
	return parser

def main():
	
	global debug
	global dry_run
	
	parser = get_argument_parser()
	args   = parser.parse_args()
	
	# sort out globals
	dry_run = args.dry_run
	
	logging.basicConfig(level=args.log_level, format='%(message)s')
	
	temp_files = []
	if not sys.stdin.isatty():
		temp_file = '/tmp/' + getpass.getuser() + ".plot.stdin." + time.strftime('%Y%m%d%H%M%S')
		logging.debug("Input stream detected, creating temporary file: " + temp_file)
		f = open(temp_file,'w')
		for line in sys.stdin:
			f.write(line.strip() + '\n')
		f.close()
		args.input.append(temp_file)
		temp_files.append(temp_file)
	
	# Any additional arguments that happen to be files are to be considered as input files
	for remaining_arg in args.remaining_args:
		if os.path.isfile(remaining_arg):
			logging.debug("Including " + remaining_arg + " as input parameter")
			args.input.append(remaining_arg)
	
	# If the input has leading spaces, for example in the case of using "uniq -c", then
	# create a temp file trimming the data before forwarding to gnuplot.
	# Do the same if the first line looks like it is coming from the command line or can
	# otherwise be ignored, e.g. header, in which case skip the first line.
	cmdlineregex = re.compile("^.*\$.*$") 
	itr = 0
	for file in args.input:
		with open(file, 'r') as f:
			line = f.readline()
			if line.startswith(" ") or cmdlineregex.match(line):
				temp_file = '/tmp/' + getpass.getuser() + ".plot." + str(itr) + "." + time.strftime('%Y%m%d%H%M%S')
				t = open(temp_file,'w')
				# whether or not to include the first line or not, skip if it looks like it is
				# coming from the command line
				if cmdlineregex.match(line) == None:
					t.write(line.strip() + '\n')
				for line in f:
					t.write(line.strip() + '\n')
				t.close()
				logging.debug("Replacing " + file + " with " + temp_file + " as input parameter")
				args.input[itr] = temp_file
				temp_files.append(temp_file)
		itr += 1
			
	if len(args.input) == 0:
		parser.print_help()
	else:
		generate_plot(args)
		
	if args.keep_temporary_files == False:
		for temp_file in temp_files:
			os.remove(temp_file)
			logging.debug("Deleted temporary file: " + temp_file)

if __name__ == "__main__":
	try:
		main()
	except (KeyboardInterrupt):
		print "\nExiting...\n"